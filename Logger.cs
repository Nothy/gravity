﻿namespace MTDemo
{
    internal  static class Logger
  {
    private static NLog.Logger logger;

    public static void Initiate()
    {
      logger = NLog.LogManager.GetCurrentClassLogger();
    }

    public static void Log(string message)
    {
      logger.Debug(message);
    }

    public static void LogException(string message)
    {
      logger.Error(message);
    }
  }
}
