﻿using System;
using System.Linq;
using MTDemo.GravityArea;
using MTDemo.Scene;

namespace MTDemo
{
  class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Logger.Initiate();
      ActorsViewModel.Initiate();
      ControlModel.Initiate();
      GravityService.Initiate();

      using (var g = new SceneView())
        g.Run(Configuration.Scene.VIEW_UPDATE_INTERVAL / 1000);

      //Console.ReadKey();
    }
  }
}
