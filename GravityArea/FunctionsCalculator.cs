﻿using System;
using System.Collections.Generic;

namespace MTDemo.GravityArea
{
  internal static class FunctionsCalculator
  {
    public static Dictionary<int, double> GetTable(double minValue, double maxValue, IFunction function)
    {
      double translationOffset, multiOffset;

      if (!double.IsNaN(function.FunctionMinValue) && !double.IsNaN(function.FunctionMaxValue))
        multiOffset = (function.FunctionMaxValue - function.FunctionMinValue) / (maxValue - minValue);
      else
        multiOffset = 1;

      if (!double.IsNaN(function.FunctionMinValue))
        translationOffset = function.FunctionMinValue - minValue;
      else
        translationOffset = 0;

      var minValueInt = (int)Math.Floor(minValue * 100);
      var maxValueInt = (int)Math.Floor(maxValue * 100);
      var result = new Dictionary<int, double>();
      for (var i = minValueInt; i <= maxValueInt; i++)
      {
        var argument = i * 0.01f * multiOffset + translationOffset;
        result.Add(i, function.Execute(argument));
      }

      return result;
    }

    internal interface IFunction
    {
      float Execute(double argument);
      double FunctionMinValue { get; }
      double FunctionMaxValue { get; }
    }

    internal class Function
    {
      public Function(float requestMinValue, float requestMaxValue, IFunction function)
      {
        if (!double.IsNaN(function.FunctionMinValue) && !double.IsNaN(function.FunctionMaxValue))
          multiOffset = (function.FunctionMaxValue - function.FunctionMinValue) / (requestMaxValue - requestMinValue);
        else
          multiOffset = 1;

        if (!double.IsNaN(function.FunctionMinValue))
          translationOffset = function.FunctionMinValue - requestMinValue;
        else
          translationOffset = 0;

        this.function = function;
      }

      private readonly double translationOffset;
      private readonly double multiOffset;
      private readonly IFunction function;

      public float Execute(double argument)
      {
        var offsetedArgument = argument * multiOffset + translationOffset;
        var result = function.Execute(offsetedArgument);
        return result;
      }
    }

    internal class SinFunction : IFunction
    {
      public float Execute(double argument)
      {
        var result = Math.Sin(argument) * (FUNCTION_MAX_VALUE - argument) / (FUNCTION_MAX_VALUE - FUNCTION_MIN_VALUE);

        return (float)result;
      }

      public const double FUNCTION_MIN_VALUE  = -Math.PI / 2;
      public const double FUNCTION_MAX_VALUE = Math.PI / 2 * 2;

      public double FunctionMinValue { get; } = FUNCTION_MIN_VALUE;
      public double FunctionMaxValue { get; } = FUNCTION_MAX_VALUE;
    }

    internal class NewtonGravityFunction : IFunction
    {
      public float Execute(double argument)
      {
        var result = 1 / Math.Pow(argument, 1.01);
        return (float)result;
      }

      public double FunctionMinValue { get; } = double.NaN;
      public double FunctionMaxValue { get; } = double.NaN;
    }

    internal class Newton2GravityFunction : IFunction
    {
      public float Execute(double argument)
      {
        float result;

        if (argument < Configuration.Gravity.MAX_IMPACT_DISTANCE / 10)
          result = (float)(argument - Configuration.Gravity.MAX_IMPACT_DISTANCE / 10);
        else
          result = (float)(1 / (argument * argument));

        return result;
      }

      public double FunctionMinValue { get; } = double.NaN;
      public double FunctionMaxValue { get; } = double.NaN;
    }

    internal class StepGravityFunction : IFunction
    {
      public float Execute(double argument)
      {
        float result;

        result = -(int)(argument / 50) % 2 * 2 - 1;

        return result;
      }

      public double FunctionMinValue { get; } = double.NaN;
      public double FunctionMaxValue { get; } = double.NaN;
    }

    internal class SqrtFunction : IFunction
    {
      public float Execute(double argument)
      {
        var result = (float)Math.Sqrt(argument);
        return result;
      }

      public double FunctionMinValue { get; } = double.NaN;
      public double FunctionMaxValue { get; } = double.NaN;
    }
  }
}
