﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Timers;
using ActorsModel = MTDemo.Services.ActorsModel;

namespace MTDemo.GravityArea
{
  internal static class GravityService
  {
    private static Timer timer;
    private static ActorsModel actors;
    private static Stopwatch watcher;

    private static GravityCalculateHandler[] handlers;

    public static void Initiate()
    {
      GravityCalculateTaskParam.Actors = Scene.ActorsViewModel.ActorsView;

      handlers = new GravityCalculateHandler[Configuration.Gravity.GRAVITY_CALCULATE_PARALLELISM];
      for (var i = 0; i < Configuration.Gravity.GRAVITY_CALCULATE_PARALLELISM; i++)
      {
        var param = new GravityCalculateTaskParam();

        param.StartIndex = Configuration.ActorsLimitation.COUNT / Configuration.Gravity.GRAVITY_CALCULATE_PARALLELISM * i;
        param.EndIndex = Configuration.ActorsLimitation.COUNT / Configuration.Gravity.GRAVITY_CALCULATE_PARALLELISM * (i + 1) - 1;

        handlers[i] = new GravityCalculateHandler(param);
      }

      actors = Scene.ActorsViewModel.ActorsView;
      watcher = new Stopwatch();

      timer = new Timer();
      timer.Interval = Configuration.Gravity.GRAVITY_UPDATE_INTERVAL;
      timer.Elapsed += TimerTick;
      timer.Enabled = true;

      watcher = new Stopwatch();
    }

    private static void TimerTick(object sender, ElapsedEventArgs elapsedEventArgs)
    {
      timer.Elapsed -= TimerTick;
      watcher.Restart();

      var tasks = new Task[Configuration.Gravity.GRAVITY_CALCULATE_PARALLELISM];
      for (var i = 0; i < Configuration.Gravity.GRAVITY_CALCULATE_PARALLELISM; i++)
      {
        var i1 = i;
        tasks[i] = Task.Run(() => handlers[i1].Execute());
      }

      Task.WaitAll(tasks);

      for (var i = 0; i < Configuration.ActorsLimitation.COUNT * 3; i = i + 3)
      {
        actors.Coordinate[Configuration.ActorPropertiesOffset.X + i] += actors.Inertia[Configuration.ActorPropertiesOffset.XINERTIA + i];
        actors.Coordinate[Configuration.ActorPropertiesOffset.Y + i] += actors.Inertia[Configuration.ActorPropertiesOffset.YINERTIA + i];
        actors.Coordinate[Configuration.ActorPropertiesOffset.Z + i] += actors.Inertia[Configuration.ActorPropertiesOffset.ZINERTIA + i];
      }

      watcher.Stop();

      Logger.Log($"Gravity calculate: {watcher.ElapsedMilliseconds:F2}");

      timer.Elapsed += TimerTick;
    }
  }
}
