﻿using System.Collections.Generic;
using MTDemo.Services;

namespace MTDemo.GravityArea
{
  internal class GravityCalculateTaskParam
  {
    public int StartIndex, EndIndex;
    public static ActorsModel Actors;

    public const float MAX_SQUARED_DISTANCE = Configuration.Gravity.MAX_IMPACT_DISTANCE * Configuration.Gravity.MAX_IMPACT_DISTANCE;

    public static Dictionary<int, float> GravityTable;
    public static Dictionary<int, float> SqrtTable;
  }
}
