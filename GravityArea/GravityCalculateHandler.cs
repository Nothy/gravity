﻿using System;
using MTDemo.Scene;

// ReSharper disable TooWideLocalVariableScope

namespace MTDemo.GravityArea
{
    internal class GravityCalculateHandler
    {
        private readonly GravityCalculateTaskParam param;
        private readonly FunctionsCalculator.Function gravityFunction;
        private static float[] coordinates;
        private static float[] ineratias;

        public GravityCalculateHandler(GravityCalculateTaskParam param)
        {
            this.param = param;
            coordinates = GravityCalculateTaskParam.Actors.Coordinate;
            ineratias = GravityCalculateTaskParam.Actors.Inertia;

            gravityFunction = new FunctionsCalculator.Function(0,
              Configuration.Gravity.MAX_IMPACT_DISTANCE,
              new FunctionsCalculator.NewtonGravityFunction());

            coordinates = ActorsViewModel.ActorsView.Coordinate;
            ineratias = ActorsViewModel.ActorsView.Inertia;
        }

        public void Execute()
        {
            var deltaXInertia = 0f;
            var deltaYInertia = 0f;
            var deltaZInertia = 0f;

            var deltaX = 0f;
            var deltaY = 0f;
            var deltaZ = 0f;

            var gravityPower = 0f;
            var koiefficient = 0f;

            var squaredX = 0f;
            var squaredY = 0f;
            var squaredZ = 0f;
            var squaredSum = 0f;
            var deltaSum = 0f;

            for (var fromI = 0; fromI < Configuration.ActorsLimitation.COUNT * 3; fromI = fromI + 3)
            {
                deltaXInertia = 0f;
                deltaYInertia = 0f;
                deltaZInertia = 0f;

                for (var toI = param.StartIndex * 3; toI <= param.EndIndex * 3; toI = toI + 3)
                {
                    if (fromI == toI)
                        continue;

                    deltaX = coordinates[toI + Configuration.ActorPropertiesOffset.X] -
                             coordinates[fromI + Configuration.ActorPropertiesOffset.X];
                    deltaY = coordinates[toI + Configuration.ActorPropertiesOffset.Y] -
                             coordinates[fromI + Configuration.ActorPropertiesOffset.Y];
                    deltaZ = coordinates[toI + Configuration.ActorPropertiesOffset.Z] -
                             coordinates[fromI + Configuration.ActorPropertiesOffset.Z];

                    squaredX = deltaX * deltaX;
                    squaredY = deltaY * deltaY;
                    squaredZ = deltaZ * deltaZ;
                    squaredSum = squaredX + squaredY + squaredZ;

                    gravityPower = gravityFunction.Execute(squaredSum);
                    if (squaredSum > GravityCalculateTaskParam.MAX_SQUARED_DISTANCE ||
                        squaredSum < Configuration.Math.FLOAT_EPSILON)
                        continue;

                    koiefficient = gravityPower / squaredSum;

                    deltaXInertia += Math.Sign(deltaX) * squaredX * koiefficient;
                    deltaYInertia += Math.Sign(deltaY) * squaredY * koiefficient;
                    deltaZInertia += Math.Sign(deltaZ) * squaredZ * koiefficient;

                }

                if (Configuration.Gravity.IMPACT_ATTENUATION != 1f)
                {
                    deltaXInertia *= Configuration.Gravity.IMPACT_ATTENUATION;
                    deltaYInertia *= Configuration.Gravity.IMPACT_ATTENUATION;
                    deltaZInertia *= Configuration.Gravity.IMPACT_ATTENUATION;
                }

                ineratias[fromI + Configuration.ActorPropertiesOffset.XINERTIA] += deltaXInertia;
                ineratias[fromI + Configuration.ActorPropertiesOffset.YINERTIA] += deltaYInertia;
                ineratias[fromI + Configuration.ActorPropertiesOffset.ZINERTIA] += deltaZInertia;
            }

            if (Configuration.Gravity.UNIVERS_ZERO_GRAVITY_POWER > 0)
            {
                for (var i = param.StartIndex * 3; i <= param.EndIndex * 3; i = i + 3)
                {
                    deltaX = -coordinates[i + Configuration.ActorPropertiesOffset.X];
                    deltaY = -coordinates[i + Configuration.ActorPropertiesOffset.Y];
                    deltaZ = -coordinates[i + Configuration.ActorPropertiesOffset.Z];

                    squaredX = deltaX * deltaX;
                    squaredY = deltaY * deltaY;
                    squaredZ = deltaZ * deltaZ;

                    squaredSum = squaredX + squaredY + squaredZ;

                    koiefficient = Configuration.Gravity.UNIVERS_ZERO_GRAVITY_POWER / squaredSum;

                    deltaXInertia += Math.Sign(deltaX) * squaredX * koiefficient;
                    deltaYInertia += Math.Sign(deltaY) * squaredY * koiefficient;
                    deltaZInertia += Math.Sign(deltaZ) * squaredZ * koiefficient;

                    ineratias[i + Configuration.ActorPropertiesOffset.XINERTIA] +=
                      deltaXInertia * Configuration.Gravity.UNIVERS_ZERO_GRAVITY_POWER;
                    ineratias[i + Configuration.ActorPropertiesOffset.YINERTIA] +=
                      deltaYInertia * Configuration.Gravity.UNIVERS_ZERO_GRAVITY_POWER;
                    ineratias[i + Configuration.ActorPropertiesOffset.ZINERTIA] +=
                      deltaZInertia * Configuration.Gravity.UNIVERS_ZERO_GRAVITY_POWER;
                }
            }

            if (Configuration.Gravity.ENVIRONMENT_VISCOSITY != 1f)
            {
                for (var i = param.StartIndex * 3; i <= param.EndIndex * 3; i = i + 3)
                {
                    ineratias[i + Configuration.ActorPropertiesOffset.XINERTIA] *=
                      Configuration.Gravity.ENVIRONMENT_VISCOSITY;
                    ineratias[i + Configuration.ActorPropertiesOffset.YINERTIA] *=
                      Configuration.Gravity.ENVIRONMENT_VISCOSITY;
                    ineratias[i + Configuration.ActorPropertiesOffset.ZINERTIA] *=
                      Configuration.Gravity.ENVIRONMENT_VISCOSITY;
                }
            }
        }
    }
}
