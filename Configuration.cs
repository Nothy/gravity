﻿using System;
using OpenTK;

namespace MTDemo
{
    internal static class Configuration
    {
        internal static class ActorPropertiesOffset
        {
            public const int X = 0;
            public const int Y = 1;
            public const int Z = 2;

            public const int XINERTIA = 0;
            public const int YINERTIA = 1;
            public const int ZINERTIA = 2;
        }

        public static class ActorsLimitation
        {
            public const int COUNT = 1500;
            public const int RADUIS = 100;
        }

        public static class Math
        {
            public const float FLOAT_EPSILON = 0.001f;
        }

        public static class Scene
        {
            public const float VIEW_UPDATE_INTERVAL = 50;
            public static readonly float MAX_FAR_Z = Single.MaxValue;
        }

        public static class Control
        {
            public const float CONTROL_UPDATE_INTERVAL = 40;
            public const float MOVEMENT_ATTENUATION = 0.4f;
            public static readonly Vector3 INIT_OFFSET = new Vector3(0f, 0f, -ActorsLimitation.RADUIS * 25);
        }

        public static class Gravity
        {
            public const float GRAVITY_UPDATE_INTERVAL = 10;
            public const int GRAVITY_CALCULATE_PARALLELISM = 5;
            public const float IMPACT_ATTENUATION = .1f;
            public const float ENVIRONMENT_VISCOSITY = 1f;
            public const float UNIVERS_ZERO_GRAVITY_POWER = .05f;
            public const float MAX_IMPACT_DISTANCE = 3000;
        }
    }
}
