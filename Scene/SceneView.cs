﻿using System;
using System.Diagnostics;
using System.Timers;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace MTDemo.Scene
{
  internal class SceneView : GameWindow
  {
    public SceneView() : base(1024, 768)
    {
    }

    private static Stopwatch watcher;
    private static float[] coordinates;

    protected override void OnLoad(EventArgs onLoadArgs)
    {
      base.OnLoad(onLoadArgs);

      coordinates = ActorsViewModel.ActorsView.Coordinate;

      watcher = new Stopwatch();

      ControlModel.Initiate();

      MouseWheel += GlWindow_MouseWheel;
      MouseMove += GlWindow_OnMouseMove;

      GL.ClearColor(0f, 0f, 0f, 0f);
      GL.Enable(EnableCap.DepthTest);
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);

      GL.Viewport(ClientRectangle);
      GL.MatrixMode(MatrixMode.Projection);

      var projection = Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, Width / (float)Height, 1f, Configuration.Scene.MAX_FAR_Z);
      GL.LoadMatrix(ref projection);
    }

    private void GlWindow_OnMouseMove(object sender, MouseMoveEventArgs e)
    {
      if (Mouse.GetState().LeftButton == ButtonState.Pressed)
      {
        ControlModel.YRotate(e.XDelta);
        ControlModel.XRotate(e.YDelta);
      }
    }

    private void GlWindow_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      ControlModel.DistanceMove(e.DeltaPrecise);
    }

    protected override void OnUpdateFrame(FrameEventArgs e)
    {
      base.OnUpdateFrame(e);

      if (Keyboard.GetState()[Key.Escape])
        Exit();

      if (Keyboard.GetState()[Key.D])
        ControlModel.ToTheRight();

      if (Keyboard.GetState()[Key.A])
        ControlModel.ToTheLeft();

      if (Keyboard.GetState()[Key.W])
        ControlModel.ToTheUp();

      if (Keyboard.GetState()[Key.S])
        ControlModel.ToTheDown();

      if (Keyboard.GetState()[Key.Q])
        ControlModel.ZRotate(1);

      if (Keyboard.GetState()[Key.E])
        ControlModel.ZRotate(-1);
    }

    protected override void OnRenderFrame(FrameEventArgs e)
    {
      //watcher.Restart();

      base.OnRenderFrame(e);

      GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
      GL.MatrixMode(MatrixMode.Modelview);

      var resultMatrix = ControlModel.RotationViewMatrix * ControlModel.TranslateViewMatrix;
      GL.LoadMatrix(ref resultMatrix);

      GL.Begin(PrimitiveType.Points);
      GL.Color3(1.0f, 1.0f, 1.0f);

      for (var i = 0; i < Configuration.ActorsLimitation.COUNT * 3; i = i + 3)
      {
        GL.Vertex3(
          coordinates[Configuration.ActorPropertiesOffset.X + i] ,
          coordinates[Configuration.ActorPropertiesOffset.Y + i] , 
          coordinates[Configuration.ActorPropertiesOffset.Z + i] );
      }
      GL.End();

      SwapBuffers();

      //watcher.Stop();

      //Logger.Log($"View render: {watcher.ElapsedMilliseconds:F2}");
    }
  }
}
