﻿using MTDemo.Services;

namespace MTDemo.Scene
{
  internal static class ActorsViewModel
  {
    public static void Initiate()
    {
      ActorsView = ActorsFactory.GenerateActors();
    }

    public static ActorsModel ActorsView;

  }
}
