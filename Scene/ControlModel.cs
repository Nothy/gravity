﻿using System;
using System.Timers;
using OpenTK;

namespace MTDemo.Scene
{
  internal static class ControlModel
  {
    private static float dxTranslate, dyTranslate, dzTranslate;
    private static float dxRotateAngle, dyRotateAngle, dzRotateAngle;
    private static Timer timer;

    public static Matrix4 RotationViewMatrix;
    public static Matrix4 TranslateViewMatrix;

    public  static void Initiate()
    {
      RotationViewMatrix = Matrix4.Identity;
      TranslateViewMatrix = Matrix4.Identity * Matrix4.CreateTranslation(Configuration.Control.INIT_OFFSET);

      timer = new Timer();
      timer.Interval = Configuration.Control.CONTROL_UPDATE_INTERVAL;
      timer.Elapsed += TimerTick;
      timer.Enabled = true;
    }

    private static void TimerTick(object sender, ElapsedEventArgs elapsedEventArgs)
    {
      timer.Elapsed -= TimerTick;

      if (Math.Abs(dzTranslate) > Configuration.Math.FLOAT_EPSILON ||
          Math.Abs(dxTranslate) > Configuration.Math.FLOAT_EPSILON ||
          Math.Abs(dyTranslate) > Configuration.Math.FLOAT_EPSILON)
      {
        TranslateViewMatrix = TranslateViewMatrix * Matrix4.CreateTranslation(dxTranslate, dyTranslate, dzTranslate);
        dxTranslate *= Configuration.Control.MOVEMENT_ATTENUATION;
        dyTranslate *= Configuration.Control.MOVEMENT_ATTENUATION;
        dzTranslate *= Configuration.Control.MOVEMENT_ATTENUATION;
      }

      if (Math.Abs(dxRotateAngle) > Configuration.Math.FLOAT_EPSILON)
      {
        RotationViewMatrix = RotationViewMatrix * Matrix4.CreateRotationY(dxRotateAngle);
        dxRotateAngle *= Configuration.Control.MOVEMENT_ATTENUATION;
      }

      if (Math.Abs(dyRotateAngle) > Configuration.Math.FLOAT_EPSILON)
      {
        RotationViewMatrix = RotationViewMatrix * Matrix4.CreateRotationX(dyRotateAngle);
        dyRotateAngle *= Configuration.Control.MOVEMENT_ATTENUATION;
      }

      if (Math.Abs(dzRotateAngle) > Configuration.Math.FLOAT_EPSILON)
      {
        RotationViewMatrix = RotationViewMatrix * Matrix4.CreateRotationZ(dzRotateAngle);
        dzRotateAngle *= Configuration.Control.MOVEMENT_ATTENUATION;
      }

      timer.Elapsed += TimerTick;
    }

    public static void ToTheRight()
    {
      dxTranslate -= 0.01f * Configuration.Control.CONTROL_UPDATE_INTERVAL;
    }

    public static void ToTheLeft()
    {
      dxTranslate += 0.01f * Configuration.Control.CONTROL_UPDATE_INTERVAL;
    }

    public static void ToTheUp()
    {
      dyTranslate -= 0.01f * Configuration.Control.CONTROL_UPDATE_INTERVAL;
    }

    public static void ToTheDown()
    {
      dyTranslate += 0.01f * Configuration.Control.CONTROL_UPDATE_INTERVAL;
    }

    public static void DistanceMove(float force)
    {
      dzTranslate += 15 * force * Configuration.Control.CONTROL_UPDATE_INTERVAL;
    }

    public static void YRotate(float force)
    {
      dxRotateAngle += 0.0001f * Configuration.Control.CONTROL_UPDATE_INTERVAL * force;
    }

    public static void XRotate(float force)
    {
      dyRotateAngle += 0.0001f * Configuration.Control.CONTROL_UPDATE_INTERVAL * force;
    }

    public static void ZRotate(float force)
    {
      dzRotateAngle += 0.0001f * Configuration.Control.CONTROL_UPDATE_INTERVAL * force;
    }
  }
}
