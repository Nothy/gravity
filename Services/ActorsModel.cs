﻿namespace MTDemo.Services
{
  internal class ActorsModel
  {
    public ActorsModel()
    {
      Coordinate = new float[3 * Configuration.ActorsLimitation.COUNT];
      Inertia = new float[3 * Configuration.ActorsLimitation.COUNT];
    }

    public readonly float[] Coordinate;
    public readonly float[] Inertia;
  }
}
