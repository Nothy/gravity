﻿using System;

namespace MTDemo.Services
{
    internal static class ActorsFactory
    {
        public static ActorsModel GenerateActors()
        {
            var actors = new ActorsModel();

            var rnd = new Random();

            float xcoord;
            float ycoord;
            float zcoord;

            float xdelta;
            float ydelta;
            float zdelta;

            xdelta = Configuration.ActorsLimitation.RADUIS * 2;
            ydelta = Configuration.ActorsLimitation.RADUIS * 2;
            zdelta = Configuration.ActorsLimitation.RADUIS * 2;

            for (var i = 0; i < Configuration.ActorsLimitation.COUNT; i++)
            {
                do
                {
                    xcoord = (float)rnd.NextDouble() * xdelta - Configuration.ActorsLimitation.RADUIS;
                    ycoord = (float)rnd.NextDouble() * ydelta - Configuration.ActorsLimitation.RADUIS;
                    zcoord = (float)rnd.NextDouble() * zdelta - Configuration.ActorsLimitation.RADUIS;
                } while (Math.Sqrt(xcoord * xcoord + ycoord * ycoord + zcoord * zcoord) > Configuration.ActorsLimitation.RADUIS);

                actors.Coordinate[Configuration.ActorPropertiesOffset.X + i * 3] = xcoord;
                actors.Coordinate[Configuration.ActorPropertiesOffset.Y + i * 3] = ycoord;
                actors.Coordinate[Configuration.ActorPropertiesOffset.Z + i * 3] = zcoord;
            }

            return actors;
        }
    }
}

